FROM node:latest

RUN mkdir -p usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8000

ENV MONGO_URI=mongodb://mongo:27017/hospital

CMD ["npm", "start"]