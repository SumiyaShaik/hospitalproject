const hospital = require('./hospital')
const employee = require('./employee')

module.exports = { hospital, employee }