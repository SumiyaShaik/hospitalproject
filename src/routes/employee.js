const express = require('express')
const path = require('path')

const Appointment = require('../models/Appointment')
const Ride = require('../models/Ride')
const { employeeLogin } = require('../utilities/login')
const { readToken, writeToken } = require('../utilities/jwt')

const router = express.Router()

router.use('/static', express.static(path.join(__dirname, '..', 'static')))

const addEmailToRequest = async (req) => {
    const token = req.cookies['token']
    if (token) {
        const { email, type, hospital } = await readToken(token)
        if (email && type === 'employee') {
            req.email = email
            req.hospital = hospital
        }
    }
}

router.use(async (req, res, next) => {
    await addEmailToRequest(req)
    next()  
})

router.get('/', (req, res) => {
    if (req.email)
        res.redirect('/employee/home')
    else
        res.render('employee__login')
})

router.post('/', async (req, res) => {
    const { email, password } = req.body
    const employee = await employeeLogin(email, password)
    if (employee) {
        const token = writeToken({ email: employee.email, type: 'employee', hospital: employee.hospital })
        res.cookie('token', token)
        addEmailToRequest(req)
        res.redirect('/employee/home')
    }
    else
        res.render('employee__login', { error: 'Wrong credentials' })
})

router.get('/home', async (req, res) => {
    if (req.email)
        res.render('employee__home')
    else
        res.redirect('/employee')
})

router.post('/book-ride', async (req, res) => {
    if (!req.email)
        return res.redirect('/')
    const ride = new Ride({ ...req.body, employee: req.email })
    try {
        await ride.save()
        res.render('employee__response', { success: true })
    } catch {
        res.render('employee__response', { success: false })
    }
})


router.get('/appointments', async (req, res) => {
    if (!req.email)
        return res.redirect('/')
    let appointments = await Appointment.find({ hospital: req.hospital })
    appointments = appointments.map(a => a.toObject())
    res.render('employee__appointments', { appointments })
})


module.exports = router