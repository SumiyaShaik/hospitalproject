const express = require('express')
const path = require('path')
const bcrypt = require('bcrypt')

const router = express.Router()

const Employee = require('../models/Employee')
const Appointment = require('../models/Appointment')
const { hospitalLogin } = require('../utilities/login')
const { readToken, writeToken } = require('../utilities/jwt')

router.use('/static', express.static(path.join(__dirname, '..', 'static')))

const addEmailToRequest = async (req) => {
    const token = req.cookies['token']
    if (token) {
        const { email, type } = await readToken(token)
        if (email && type === 'hospital') {
            req.email = email
        }
    }
}

router.use(async (req, res, next) => {
    await addEmailToRequest(req)
    next()  
})

router.get('/', (req, res) => {
    if (req.email)
        res.redirect('/hospital/home')
    else
        res.render('hospital__login')
})

router.post('/', async (req, res) => {
    const { email, password } = req.body
    const hospital = await hospitalLogin(email, password)
    if (hospital) {
        const token = writeToken({ email: hospital.email, type: 'hospital'})
        res.cookie('token', token)
        addEmailToRequest(req)
        res.redirect('/hospital/home')
    }
    else
        res.render('hospital__login', { error: 'Wrong credentials' })
})

/* 2. Hospital home page */
router.get('/home', async (req, res) => {
    if (req.email)
        res.render('hospital__home')
    else
        res.redirect('/hospital')
})

/* 3. Add patient POST endpoint (Displays whether patient registration is successful) */
router.post('/add-appointment', async (req, res) => {
    if (!req.email)
        return res.redirect('/')
    const appointment = new Appointment({ ...req.body, hospital: req.email })
    try {
        await appointment.save()
        res.render('hospital__response', { message: 'APPOINTMENT ADDED' })
    } catch {
        res.render('hospital__response', { message: 'SOME ERROR OCCURED' })
    }
})

router.get('/add-employee', (req, res) => {
    if (!req.email)
        return res.redirect('/')
    res.render('hospital__add-employee')
})

router.post('/add-employee', async (req, res) => {
    if (!req.email)
        return res.redirect('/')
    const { email, password } = req.body

    const existing = await Employee.findOne({ email })
    if (existing)
        return res.render('hospital__add-employee', { error: 'Email already in use' })

    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(password, salt)

    const employee = new Employee({ email, password: hashedPassword, hospital: req.email })    
    await employee.save()
    res.render('hospital__response', { message: 'EMPLOYEE ADDED' })

})


module.exports = router