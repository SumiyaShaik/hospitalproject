const express = require('express')
const path = require('path')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const cookieParser = require('cookie-parser')

const Hospital = require('./models/Hospital')
const { writeToken } = require('./utilities/jwt')

const app = express()
const PORT = process.env.PORT || 8000
const MONGO_URI = process.env.MONGO_URI || 'mongodb://localhost:27017/hospital'
mongoose.connect(MONGO_URI, (error) => {
    if (error)
        console.log(error)
    else
        console.log('Database connected')
})

/* Config */
app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'views'))

/* Middleware */
app.use(cookieParser())
app.use(express.urlencoded({ extended: true })) 
app.use('/static', express.static(path.join(__dirname, 'static')))

/* Routes */

app.get('/', (req, res) => res.render('index'))

app.post('/', async (req, res) => {
    const { email, password } = req.body

    const existing = await Hospital.findOne({ email })
    if (existing)
        return res.render('index', { error: 'Email already in use' })
    
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(password, salt)
    
    const hospital = new Hospital({ email, password: hashedPassword })
    await hospital.save()
    const token = await writeToken({ email, type: 'hospital' })
    res.cookie('token', token)

    res.redirect('/hospital')

})

app.get('/logout', (req, res) => res.clearCookie('token').redirect('/'))

const routes = require('./routes')

app.use('/hospital', routes.hospital)
app.use('/employee', routes.employee)

app.listen(PORT, error => {
    console.log('Server starting')
    if (error)
        console.log('Error:', error)
    else
        console.log('Success')
})