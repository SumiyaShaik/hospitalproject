const { Schema, model } = require('mongoose')

const schema = new Schema({
    email: String,
    password: String,
    hospital: String
})

module.exports = model('Employee', schema)