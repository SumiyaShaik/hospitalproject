const { Schema, model } = require('mongoose')

const schema = new Schema({
    patient_name: String,
    date: String,
    time: String,
    address: String,
    contact_number: Number,
    hospital: String
})

module.exports = model('Appointment', schema)