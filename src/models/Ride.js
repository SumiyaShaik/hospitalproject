const { Schema, model } = require('mongoose')

const schema = new Schema({
    pickup_location: String,
    date: String,
    time: String,
    destination: String,
    customer_name: String,
    customer_contact_number: Number,
    employee: String
})

module.exports = model('Ride', schema)