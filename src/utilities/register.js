const bcrypt = require('bcrypt')

const Hospital = require('../models/Hospital')
const { writeToken } = require('./jwt')

const hospitalRegister = async (email, password) => {
    const existing = await Hospital({ email })
    if (existing)
        return res.render('index', { error: 'Email already in use' })
    
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(password, salt)

    const hospital = new Hospital({ email, hashedPassword })
    await hospital.save()
    const token = await writeToken({ email, type: 'hospital' })

    res.cookie('token', token)

}