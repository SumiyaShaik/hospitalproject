const bcrypt = require('bcrypt')

const Employee = require('../models/Employee')
const Hospital = require('../models/Hospital')

const employeeLogin = async (email, password) => {
    const employee = await Employee.findOne({ email })  
    if (!employee)
        return  
    if (await bcrypt.compare(password, employee.password))
        return employee.toObject()  
    return
}

const hospitalLogin = async (email, password) => {
    const hospital = await Hospital.findOne({ email })
    if (!hospital)
        return
    if (await bcrypt.compare(password, hospital.password))
        return hospital.toObject()
    return
}

module.exports = { employeeLogin, hospitalLogin }