const jwt = require('jsonwebtoken')

const { JWT_SECRET } = require('../constants')

const readToken = async (token) => {
    try {
        const data = await jwt.verify(token, JWT_SECRET)
        return data
    } catch {
        return
    }
}

const writeToken = (data) => {
    const token = jwt.sign(data, JWT_SECRET)
    return token
}

module.exports = { readToken, writeToken }